package com.example.myapplication1

import retrofit2.Call
import retrofit2.http.GET

interface API {

    @GET("meta/")
    fun getData() : Call<List<GottenObject>>
}