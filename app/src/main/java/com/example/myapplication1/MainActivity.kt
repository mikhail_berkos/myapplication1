package com.example.myapplication1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://test.clevertec.ru/tt/").build()
            .create(API::class.java)
        val retrofitData = retrofit.getData()

        retrofitData.enqueue(object : Callback<List<GottenObject>?> {
            override fun onResponse(
                call: Call<List<GottenObject>?>,
                response: Response<List<GottenObject>?>
            ) {
                val responseBody = response.body()!!
                for (myData in responseBody){
                    Log.e("AAA", myData.name.toString())

                }
            }

            override fun onFailure(call: Call<List<GottenObject>?>, t: Throwable) {

            }
        })

        setContentView(R.layout.activity_main)
    }
}